var map = null;
var heatmap = null;
var markers = [];
var airports = {};

function init() {
	$.getJSON("/api/simulation/status", function(data, status, xhr) {
		if (!data.running) {
			setError("Simulation is not running !")
			return;
		}

		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 3,
			// Center on Paris
			center: {lat: 48.8566, lng: 2.3522}
		});
	
		/*google.maps.event.addListener(map, 'zoom_changed', function() {
			var zoom = map.getZoom();
			for (var marker of markers) {
				if (!marker.isImportant) {
					marker.setVisible(zoom > 4);
				}
			}
		});*/
	
		$.getJSON("/api/data/airports", function(data, status, xhr) {
			airports = data;
			for (var airportID in airports) {
				var airport = airports[airportID];

				let marker = new google.maps.Marker({
					map,
					position: {
						lat: airport.latitude,
						lng: airport.longitude
					},
					icon: "/public/img/airport_icon_10px.png"
				});
				markers[airportID] = marker;
			}

			$.getJSON("/api/data/snapshots/airports", function(data, status, xhr) {
				var heatMapData = [];
				for (let airportID in data) {
					let airport = airports[airportID];
					let airportData = data[airportID];
					if (airport) {
						heatMapData.push({
							location: new google.maps.LatLng(airport.latitude, airport.longitude),
							weight: airportData.passengers
						});
					} else {
						console.warn(`Unknown airport with id '${airportID}'`);
					}
					
					let destinations = (airport.destinations)
						? airport.destinations.map((id) => airports[id].name)
						: "Aucune";

					let marker = markers[airportID]
					marker.addListener('click', function() {
						$("#airportDetails").html(`
							<p><strong>${airport.name}</strong></p>
							<div class="table-responsive">
								<table class="table">
									<tbody>
										<tr>
											<td>Passengers</td>
											<td>${prettyPrintNumber(airportData.passengers)}</td>
										</tr>
										<tr>
											<td>Planes</td>
											<td>${prettyPrintNumber(airportData.planes)}</td>
										</tr>
										<tr>
											<td>Destinations</td>
											<td>${prettyPrintNumber(destinations.length)}</td>
										</tr>
									</tbody>
								</table>
							</div>
						`);
					});
				}

				heatmap = new google.maps.visualization.HeatmapLayer({
					data: heatMapData,
					dissipating: false,
					radius: 1
				});
				heatmap.setMap(map);

			}).fail(function (xhr, status, err) {
				setError(xhr.responseJSON.err);
			});

			//setInterval(updateMap, 5000);

		}).fail(function (xhr, status, err) {
			setError(xhr.responseJSON.err);
		});
	}).fail(function (xhr, status, err) {
		setError(xhr.responseJSON.err);
	});
}

function updateMap() {
	console.log("updating map");
	$.getJSON("/api/data", function(data, status, xhr) {
		console.log(data);
	});
}
