/**
 * Global utils
 */

function setError(msg) {
	$("#errorContainer").html(`
		<div class="col-sm-6 col-sm-offset-3">
			<div class="alert alert-danger">Error: ${msg}</div>
		</div>
	`);
}

function prettyPrintNumber(n, i=0) {
    if (n > 1000000000) {
        return `${(n/1000000000).toFixed(i)} billion`;
    }
    if (n > 1000000) {
        return `${(n/1000000).toFixed(i)} million`;
    }
    if (n > 1000) {
        return `${(n/1000).toFixed(i)}k`;
    }
    return n.toFixed(i);
}

