var map = null;
var heatmap = null;
var markers = [];
var airports = {};

function init() {
	$.getJSON("/api/simulation/status", function(data, status, xhr) {
		if (!data.running) {
			setError("Simulation is not running !")
			return;
		}

		map = new google.maps.Map(document.getElementById('map'), {
			zoom: 3,
			// Center on Paris
			center: {lat: 48.8566, lng: 2.3522}
		});

		$.getJSON("/api/data/airports", function(data, status, xhr) {
			airports = data;
	
			$.getJSON("/api/data/routes", function(data, status, xhr) {
				for (var source in data) {
					var sourceID = parseInt(source);
					var sourceAirport = airports[sourceID];
					if (!sourceAirport) {
						setError(`Invalid airport id '${sourceID}'`);
						return;
					}
					for (var destID of data[source]) {
						var destAirport = airports[destID];
						if (!destAirport) {
							setError(`Invalid airport id '${destID}'`);
							return;
						}

						var routeLine = new google.maps.Polyline({
							path: [
								{
									lat: sourceAirport.latitude,
									lng: sourceAirport.longitude
								},
								{
									lat: destAirport.latitude,
									lng: destAirport.longitude
								}
							],
							geodesic: true,
							strokeColor: '#FF0000',
							strokeOpacity: 0.3,
							strokeWeight: 0.1
						});
						routeLine.setMap(map);
					}
				}

			}).fail(function (xhr, status, err) {
				console.log(xhr)
				setError(xhr.responseJSON.err);
			});
		}).fail(function (xhr, status, err) {
			setError(xhr.responseJSON.err);
		});
	}).fail(function (xhr, status, err) {
		setError(xhr.responseJSON.err);
	});
}
