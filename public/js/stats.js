

function init() {


    update();
    setInterval(update, 1000);
}

function update() {
    $.getJSON("/api/data/snapshots/stats", function(data, status, xhr) {
        var dt = data.t;
        if (dt < 24) {
            $("#elapsedTime").text(`${prettyPrintNumber(dt, 0)} hours`);
        } else if (dt < 24*365) {
            $("#elapsedTime").text(`${prettyPrintNumber(dt/24, 0)} days`);
        } else {
            $("#elapsedTime").text(`${prettyPrintNumber(dt/(24*365), 0)} years`);
        }

        $("#transportedPassengers").text(prettyPrintNumber(data.transportedPassengers));
        $("#flights").text(prettyPrintNumber(data.flightCount));

    }).fail(function (xhr, status, err) {
        setError(xhr.responseJSON.err);
    });
}
