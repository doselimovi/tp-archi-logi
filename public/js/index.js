const startOrStop = (action) => () => {
    $("#display").empty();
    $.ajax({
        type: 'GET',
        url: `/api/simulation/${action}`,
        error: (xhr, status, error) => {
            $("#display").html(`
                <div class="alert alert-danger">
                    Error: ${xhr.responseJSON.err}
                </div>
            `);
        },
        success: (result, status, xhr) => {
            location.reload(true);
        }
    });
}

$("#stopSimul").click(startOrStop('stop'));
$("#startSimul").click(startOrStop('start'));
