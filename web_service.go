package main

import (
	"html/template"
	"io"
	"log"
	"net/http"
	"path/filepath"

	"github.com/labstack/echo"
	"gitlab.isima.fr/doselimovi/tp-archi-logi/simul"
)

var (
	simulData = make([]byte, 0)
)

type jsonError struct {
	Err string `json:"err"`
}

type echoTemplate struct {
}

type templateData struct {
	Page    string
	Pages   map[string]string
	Running bool
}

func (e *echoTemplate) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	t := template.Must(template.ParseGlob(filepath.Join("views", "*.html")))
	return t.ExecuteTemplate(w, name, data)
}

func ensureRunning(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if !simul.Running {
			return c.JSON(http.StatusServiceUnavailable, jsonError{"Simulation not running"})
		}
		return next(c)
	}
}

func renderTemplate(name string, pages map[string]string) func(c echo.Context) error {
	return func(c echo.Context) error {
		return c.Render(http.StatusOK, name, &templateData{
			name,
			pages,
			simul.Running,
		})
	}
}

func startWebService(addr string) {
	e := echo.New()

	e.GET("/_ah/health", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})

	e.Renderer = &echoTemplate{}

	e.Static("/public", "public")
	e.File("/favicon.ico", "public/favicon.ico")

	routes := map[string]string{
		"/":         "index",
		"/airports": "airports",
		"/routes":   "routes",
		"/stats":    "stats",
	}
	for url, templ := range routes {
		e.GET(url, renderTemplate(templ, routes))
	}

	setupWebAPI(e.Group("/api"))

	e.Logger.Fatal(e.Start(addr))
}

func setupWebAPI(api *echo.Group) {

	api.GET("/simulation/start", func(c echo.Context) error {
		log.Println("Starting simulation...")
		err := simul.Start(dataDir, simulationStep)
		if err != nil {
			log.Println(err.Error())
			return c.JSON(http.StatusInternalServerError, jsonError{err.Error()})
		}
		return c.NoContent(http.StatusOK)
	})

	api.GET("/simulation/stop", func(c echo.Context) error {
		log.Println("Stopping simulation...")
		simul.Stop()
		return c.NoContent(http.StatusOK)
	})

	api.GET("/simulation/status", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]interface{}{
			"running": simul.Running,
		})
	})

	dataGroup := api.Group("/data")
	dataGroup.Use(ensureRunning)

	dataGroup.GET("/airports", func(c echo.Context) error {
		return c.JSONBlob(http.StatusOK, simul.GetAirportsDetails())
	})

	dataGroup.GET("/routes", func(c echo.Context) error {
		return c.JSONBlob(http.StatusOK, simul.GetRoutesDetails())
	})

	dataGroup.GET("/snapshots/stats", func(c echo.Context) error {
		return c.JSONBlob(http.StatusOK, simul.GetStatsSnapshot())
	})

	dataGroup.GET("/snapshots/airports", func(c echo.Context) error {
		return c.JSONBlob(http.StatusOK, simul.GetAirportsSnapshot())
	})
}
