# Builder container
FROM iron/go:dev as builder
WORKDIR /go/src/gitlab.isima.fr/doselimovi/tp-archi-logi

COPY *.go ./
COPY simul/*.go ./simul/

RUN go get -v ./...
RUN go build -o build

# Main container
FROM iron/go
WORKDIR /app

COPY --from=builder /go/src/gitlab.isima.fr/doselimovi/tp-archi-logi/build .
COPY data data
COPY public public
COPY views views

EXPOSE 8080
ENTRYPOINT [ "./build" ]
