// Package main contains all code for simulation
package main

import (
	"flag"
	"log"
	"math/rand"
	"time"

	"gitlab.isima.fr/doselimovi/tp-archi-logi/simul"
)

const (
	simulationStep = 2 * time.Hour
)

var (
	dataDir        = "data"
	webServiceAddr = flag.String("addr", ":8080", "listening address for the web service")
)

func main() {
	flag.Parse()
	rand.Seed(time.Now().Unix())

	err := simul.Start(dataDir, simulationStep)
	if err != nil {
		log.Println(err.Error())
	}

	startWebService(*webServiceAddr)
}
