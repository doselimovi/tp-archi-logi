package simul

import (
	"errors"
	"math/rand"
	"time"

	"github.com/kellydunn/golang-geo"
)

const (
	// Maximum number of passengers for a given destination
	maxcountPassengers = 100000
	// Hourly arrival rate of passengers in airports
	passengerArrivalRate = 800
)

type airport struct {
	ID           int     `json:"id"`
	Name         string  `json:"name"`
	City         string  `json:"city"`
	Country      string  `json:"country"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Destinations []int   `json:"destinations"`

	passengers map[int]int
}

// init initialize the airport variables based on fetched data
func (a *airport) init() error {
	dest, ok := routes[a.ID]
	if !ok {
		return errors.New("Airport not found in routes")
	}

	a.Destinations = make([]int, len(dest))
	copy(a.Destinations, dest)

	a.passengers = make(map[int]int)
	for _, d := range a.Destinations {
		a.passengers[d] = 0
	}

	return nil
}

func (a *airport) generatePassengers(dt time.Duration) {
	if len(a.Destinations) == 0 {
		return
	}

	n := int(rand.NormFloat64()/50 + dt.Hours()*passengerArrivalRate)
	a.receivePassengers(n)
}

func (a *airport) receivePassengers(n int) {
	for n > 0 {
		k := rand.Intn(n) + 1
		dest := a.Destinations[rand.Intn(len(a.Destinations))]

		newVal := a.passengers[dest] + k
		if newVal > maxcountPassengers {
			newVal = maxcountPassengers
		}
		a.passengers[dest] = newVal

		n -= k
	}
}

func (a *airport) update(dt time.Duration) {
	a.generatePassengers(dt)
}

func (a *airport) countPassengers() (n int) {
	for _, val := range a.passengers {
		n += val
	}
	return
}

func (a *airport) planes() int {
	n := 0
	for _, p := range planes {
		if p.location != nil && p.location.ID == a.ID {
			n++
		}
	}
	return n
}

func (a *airport) preferredDestination() *airport {
	var n, dest int
	for d, k := range a.passengers {
		if k > n {
			n = k
			dest = d
		}
	}
	if n == 0 {
		return nil
	}
	return airports[dest]
}

func (a *airport) travelTimeTo(a2 *airport) time.Duration {

	p1 := geo.NewPoint(a.Latitude, a.Longitude)
	p2 := geo.NewPoint(a2.Latitude, a2.Longitude)

	distance := p1.GreatCircleDistance(p2)

	return time.Duration(distance/0.2) * time.Second
}
