package simul

import (
	"log"
	"time"
)

const (
	// Century represents a time.Duration of one hundred year
	Century = 876000 * time.Hour
)

var (
	// Running tells wether or not the simulation is currently running
	Running = false

	elapsedTime      time.Duration
	elapsedCenturies int64
	simulDelta       time.Duration

	expUpdateRate = 300 * time.Millisecond

	transportedPassengers = 0
	flightCount           = 0

	stopChan = make(chan int)

	airports map[int]*airport
	planes   map[int]*plane
	routes   map[int][]int
)

// Start ...
func Start(dataDir string, dt time.Duration) error {
	if Running {
		log.Println("Simulation is already running")
		return nil
	}

	log.Println("Loading data...")
	err := loadData(dataDir)
	if err != nil {
		return err
	}

	// Initialize airports
	log.Println("Initializing airports...")
	for _, a := range airports {
		a.init()
	}

	// Initialize data
	elapsedTime = 0
	elapsedCenturies = 0
	simulDelta = dt
	transportedPassengers = 0
	flightCount = 0
	initExported()

	log.Println("Launching simulation...")
	go run()
	Running = true

	return nil
}

// Stop ...
func Stop() {
	stopChan <- 0
	Running = false
}

func run() {
	t := time.Now()

	for {
		select {
		case _ = <-stopChan:
			log.Println("Simulation goroutine received stop signal")
			return
		default:
			update(simulDelta)
		}

		if time.Since(t) > expUpdateRate {
			t = time.Now()
			updateExported()
		}

		time.Sleep(1 * time.Millisecond)
	}
}

func update(dt time.Duration) {
	for _, a := range airports {
		a.update(dt)
	}
	for _, p := range planes {
		p.update(dt)
	}

	elapsedTime += dt
	// if elapsedTime duration is more than 100 years
	if elapsedTime > Century {
		elapsedCenturies++
		elapsedTime -= Century
	}
}
