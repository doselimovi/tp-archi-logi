package simul

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"path/filepath"
)

func deleteAirport(id int) {
	delete(airports, id)
	delete(routes, id)
	for airportID, destinations := range routes {
		for i := len(destinations) - 1; i >= 0; i-- {
			if destinations[i] == id {
				destinations[i] = destinations[len(destinations)-1]
				destinations = destinations[:len(destinations)-1]
				routes[airportID] = destinations
			}
		}
	}
}

func cleanRoutes() {
	for id, destinations := range routes {
		_, ok := airports[id]
		if !ok {
			delete(routes, id)
			continue
		}
		for i := len(destinations) - 1; i >= 0; i-- {
			_, ok = airports[destinations[i]]
			if !ok {
				destinations[i] = destinations[len(destinations)-1]
				destinations = destinations[:len(destinations)-1]
				routes[id] = destinations
			}
		}
	}
}

func cleanAirports() {
	// Delete airports with no destination
	needToReiterate := true
	for needToReiterate {
		needToReiterate = false
		for id := range airports {
			destinations, ok := routes[id]
			if !ok || len(destinations) == 0 {
				deleteAirport(id)
				needToReiterate = true
			}
		}
	}
}

func loadData(dataDir string) (err error) {
	raw, err := ioutil.ReadFile(filepath.Join(dataDir, "airports.json"))
	if err != nil {
		return
	}
	err = loadAirports(raw)
	if err != nil {
		return
	}

	raw, err = ioutil.ReadFile(filepath.Join(dataDir, "routes.json"))
	if err != nil {
		return
	}
	err = loadRoutes(raw)
	if err != nil {
		return
	}

	cleanRoutes()
	cleanAirports()

	raw, err = ioutil.ReadFile(filepath.Join(dataDir, "planes.json"))
	if err != nil {
		return
	}
	err = loadPlanes(raw)
	if err != nil {
		return
	}
	airportIDs := make([]int, 0, len(airports))
	for id := range airports {
		airportIDs = append(airportIDs, id)
	}
	for _, p := range planes {
		airportID := airportIDs[rand.Intn(len(airportIDs))]
		p.location = airports[airportID]
	}

	return
}

func loadAirports(rawContent []byte) (err error) {
	var allAirports []*airport
	err = json.Unmarshal(rawContent, &allAirports)
	if err != nil {
		return
	}

	airports = make(map[int]*airport, len(allAirports))
	for _, a := range allAirports {
		airports[a.ID] = a
	}

	return
}

func loadPlanes(rawContent []byte) (err error) {
	var allPlanes []struct {
		Model    string `json:"model"`
		Capacity int    `json:"capacity"`
		Quantity int    `json:"quantity"`
	}
	err = json.Unmarshal(rawContent, &allPlanes)
	if err != nil {
		return
	}

	planes = make(map[int]*plane, len(allPlanes))
	for i, p := range allPlanes {
		for j := 0; j < p.Quantity; j++ {
			id := j*len(allPlanes) + i
			planes[id] = &plane{
				ID:       id,
				Model:    p.Model,
				Capacity: p.Capacity,
			}
		}
	}

	return
}

func loadRoutes(rawContent []byte) (err error) {
	routes = make(map[int][]int)

	var allRoutes []struct {
		ID     int `json:"id"`
		Source int `json:"source"`
		Dest   int `json:"dest"`
	}
	err = json.Unmarshal(rawContent, &allRoutes)
	if err != nil {
		return
	}

	for _, route := range allRoutes {
		destinations, ok := routes[route.Source]
		if !ok {
			routes[route.Source] = []int{route.Dest}
		} else {
			exists := false
			for _, dest := range destinations {
				if dest == route.Dest {
					exists = true
				}
			}
			if !exists {
				routes[route.Source] = append(destinations, route.Dest)
			}
		}
	}

	return
}
