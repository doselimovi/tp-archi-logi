package simul

import (
	"path/filepath"
	"testing"
)

func TestStart(t *testing.T) {
	err := Start(filepath.Join("..", "data"), simulDelta)
	if err != nil {
		t.Fatal(err.Error())
	}

	for id, a := range airports {
		if id != a.ID {
			t.Fatalf("Airport with id %v has a different key in airports map (%v)", a.ID, id)
		}
	}

	for source, destinations := range routes {
		_, ok := airports[source]
		if !ok {
			t.Fatalf("Unknown airport %v as source in routes", source)
		}

		for _, dest := range destinations {
			_, ok = airports[dest]
			if !ok {
				t.Fatalf("Unknown airport %v as dest for source %v in routes", dest, source)
			}
		}
	}
}
