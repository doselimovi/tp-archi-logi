package simul

import (
	"time"
)

const (
	flightDuration   = 10 * time.Hour
	onGroundDuration = 1 * time.Hour
)

// Plane ...
type plane struct {
	ID         int
	Model      string
	Capacity   int
	Passengers int

	location *airport
	target   *airport

	remainingTime time.Duration
}

func (p *plane) update(dt time.Duration) {
	if p.remainingTime > 0 {
		p.remainingTime -= dt
		return
	}

	if p.location != nil {
		// Plane is on land
		p.target = p.location.preferredDestination()
		if p.target == nil {
			// TODO plane has nowhere to go
			return
		}
		p.Passengers = p.Capacity
		if p.location.passengers[p.target.ID] < p.Capacity {
			p.Passengers = p.location.passengers[p.target.ID]
		}
		p.location.passengers[p.target.ID] -= p.Passengers

		p.remainingTime = p.location.travelTimeTo(p.target)
		p.location = nil

	} else {
		// Plane is in the air
		p.location = p.target
		p.target = nil

		transportedPassengers += p.Passengers
		flightCount++

		p.Passengers = 0
		p.remainingTime = onGroundDuration
	}
}
