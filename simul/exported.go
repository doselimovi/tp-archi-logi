package simul

import (
	"encoding/json"
	"sync"
)

var (
	expAiportsDetails   []byte
	expRoutesDetails    []byte
	expStatsSnapshot    []byte
	expAirportsSnapshot []byte
	mutex               sync.RWMutex
)

type simulSnapshot struct {
	T                     float64 `json:"t"`
	TransportedPassengers int     `json:"transportedPassengers"`
	FlightCount           int     `json:"flightCount"`
}

type airportSnapshot struct {
	Passengers int `json:"passengers"`
	Planes     int `json:"planes"`
}

type simulDetails struct {
	Aiports map[int]*airport `json:"airports"`
	Planes  map[int]*plane   `json:"planes"`
	Routes  map[int][]int    `json:"routes"`
}

func initExported() {
	// init simulation details
	expAiportsDetails, _ = json.Marshal(airports)
	expRoutesDetails, _ = json.Marshal(routes)

	// init the rest
	updateExported()
}

func updateExported() {
	mutex.Lock()
	defer mutex.Unlock()

	// update simulation snapshot
	expStatsSnapshot, _ = json.Marshal(&simulSnapshot{
		T: elapsedTime.Hours() + float64(elapsedCenturies)*Century.Hours(),
		TransportedPassengers: transportedPassengers,
		FlightCount:           flightCount,
	})

	// update airports snapshot
	airportSnapshots := make(map[int]*airportSnapshot, len(airports))
	for id, a := range airports {
		airportSnapshots[id] = &airportSnapshot{
			Passengers: a.countPassengers(),
			Planes:     a.planes(),
		}
	}
	expAirportsSnapshot, _ = json.Marshal(airportSnapshots)
}

func getData(exported []byte) []byte {
	mutex.RLock()
	res := make([]byte, len(exported), len(exported))
	copy(res, exported)
	mutex.RUnlock()
	return res
}

// GetAirportsDetails ...
func GetAirportsDetails() []byte {
	return getData(expAiportsDetails)
}

// GetRoutesDetails ...
func GetRoutesDetails() []byte {
	return getData(expRoutesDetails)
}

// GetStatsSnapshot ...
func GetStatsSnapshot() []byte {
	return getData(expStatsSnapshot)
}

// GetAirportsSnapshot ...
func GetAirportsSnapshot() []byte {
	return getData(expAirportsSnapshot)
}

/*
func initExportedData() {
	expData = &exportedData{
		T:        elapsedTime,
		Airports: make(map[int]*airportState),
	}
}

func updateExportedData() {
	// Update exported data
	expData.T = elapsedTime
	expData.TransportedPassengers = transportedPassengers
	expData.FlightCount = flightCount

	for id, a := range airports {
		expAirport, ok := expData.Airports[id]
		if !ok {
			expAirport = &airportState{
				ID: a.ID,
			}
			expData.Airports[id] = expAirport
		}
		expAirport.Passengers = a.countPassengers()
		expAirport.Planes = a.planes()
	}

	mutex.Lock()
	defer mutex.Unlock()
	expDataBytes, _ = json.Marshal(expData)

	// Update exported airports
	expAirportsBytes, _ = json.Marshal(airports)

	// Update exported routes
	expRoutesBytes, _ = json.Marshal(routes)

}

func getData(exported []byte) []byte {
	mutex.RLock()
	res := make([]byte, len(exported), len(exported))
	copy(res, exported)
	mutex.RUnlock()
	return res
}

// GetExportedData ...
func GetExportedData() []byte {
	return getData(expDataBytes)
}

// GetAirportsData ...
func GetAirportsData() []byte {
	return getData(expAirportsBytes)
}

// GetRoutesData ...
func GetRoutesData() []byte {
	return getData(expRoutesBytes)
}
*/
