package simul

import (
	"testing"
	"time"
)

func TestAirportInit(t *testing.T) {
	a := &airport{
		ID: 0,
	}

	routes = map[int][]int{
		a.ID: []int{1, 2, 3},
	}

	a.init()

	for _, targetDest := range routes[a.ID] {
		containsDestination := false
		for _, dest := range a.Destinations {
			if dest == targetDest {
				containsDestination = true
			}
		}
		if !containsDestination {
			t.Fatal("Airport does not contain expected destination")
		}

		_, ok := a.passengers[targetDest]
		if !ok {
			t.Fatal("Airport does not have passenger count for destination")
		}
	}

}

func TestPassengersGeneration(t *testing.T) {
	a := &airport{
		ID: 0,
	}

	routes = map[int][]int{
		a.ID: []int{1},
	}

	a.init()

	a.generatePassengers(time.Hour)

	if a.countPassengers() < passengerArrivalRate/2 {
		t.Fatal("Airport should have more passengers")
	}
}

func TestPreferredDestination(t *testing.T) {
	airports = map[int]*airport{
		1: &airport{},
	}
	a := &airport{
		passengers: map[int]int{
			0: 5,
			1: 8,
			2: 6,
		},
	}

	prefAirport := a.preferredDestination()
	if prefAirport != airports[1] {
		t.Fatalf("Preferred destination should be %p and not %p", airports[1], prefAirport)
	}
}

func TestTravelTimeTo(t *testing.T) {
	paris := &airport{
		ID:        0,
		Latitude:  49.009691,
		Longitude: 2.547925,
	}
	newYork := &airport{
		ID:        0,
		Latitude:  40.641311,
		Longitude: -73.778139,
	}

	expectedTravelTime := time.Duration(29167000000000)

	if paris.travelTimeTo(newYork) != newYork.travelTimeTo(paris) {
		t.Fatal("Travel time should be symmetric")
	}
	if paris.travelTimeTo(newYork) != expectedTravelTime {
		t.Fatalf("Travel time should be %v\n", expectedTravelTime)
	}
}
