package simul

import (
	"testing"
)

func assertRoutesEquals(t *testing.T, expectedRoutes map[int][]int) {
	for id, expDest := range expectedRoutes {
		destinations, ok := routes[id]
		if !ok {
			t.Fatalf("Routes should have airport %v as source", id)
		}
		if len(destinations) != len(expDest) {
			t.Fatalf("Airport %v should have %v destinations (has %v)", id, len(expDest), len(destinations))
		}

		contains := func(l []int, x int) bool {
			for _, y := range l {
				if x == y {
					return true
				}
			}
			return false
		}
		for _, d := range expDest {
			if !contains(destinations, d) {
				t.Fatalf("Airport %v should have for %v destination", id, d)
			}
		}
		for _, d := range destinations {
			if !contains(expDest, d) {
				t.Fatalf("Airport %v should not have %v for destination", id, d)
			}
		}
	}
}

func assertAirportsEquals(t *testing.T, expectedAirports map[int]*airport) {
	for id := range expectedAirports {
		a, ok := airports[id]
		if !ok {
			t.Fatalf("Airports should have airport %v", id)
		}
		if a.ID != id {
			t.Fatalf("Airport %v should have ID %v", a.ID, id)
		}
	}
}

func TestDeleteAirport(t *testing.T) {
	airports = map[int]*airport{
		0: &airport{ID: 0},
		1: &airport{ID: 1},
		2: &airport{ID: 2},
		3: &airport{ID: 3},
		4: &airport{ID: 4},
	}
	routes = map[int][]int{
		0: []int{1, 1, 2, 3, 4},
		1: []int{0, 2, 3, 4},
		2: []int{0, 1, 3, 1, 4},
		3: []int{0, 1, 2, 4, 1, 1},
		4: []int{0, 1, 2, 1, 3},
	}
	toDelete := 1
	expectedAirports := map[int]*airport{
		0: airports[0],
		2: airports[2],
		3: airports[3],
		4: airports[4],
	}
	expectedRoutes := map[int][]int{
		0: []int{2, 3, 4},
		2: []int{0, 3, 4},
		3: []int{0, 2, 4},
		4: []int{0, 2, 3},
	}

	deleteAirport(toDelete)
	assertRoutesEquals(t, expectedRoutes)
	assertAirportsEquals(t, expectedAirports)
}

func TestCleanRoutes(t *testing.T) {
	airports = map[int]*airport{
		0: &airport{ID: 0},
		1: &airport{ID: 1},
		2: &airport{ID: 2},
	}
	routes = map[int][]int{
		0: []int{3},
		1: []int{0, 3, 3, 2},
		2: []int{3, 3, 0, 3, 3},
		3: []int{0, 1},
	}
	expectedRoutes := map[int][]int{
		0: []int{},
		1: []int{0, 2},
		2: []int{0},
	}

	cleanRoutes()
	assertRoutesEquals(t, expectedRoutes)
}

func TestCleanAirports(t *testing.T) {
	airports = map[int]*airport{
		0: &airport{ID: 0},
		1: &airport{ID: 1},
		2: &airport{ID: 2},
		3: &airport{ID: 3},
		4: &airport{ID: 4},
		5: &airport{ID: 5},
	}
	routes = map[int][]int{
		1: []int{0, 4},
		3: []int{0, 1, 4},
		4: []int{0, 1},
	}
	expectedAirports := map[int]*airport{
		1: airports[1],
		3: airports[3],
		4: airports[4],
	}
	expectedRoutes := map[int][]int{
		1: []int{4},
		3: []int{1, 4},
		4: []int{1},
	}

	cleanAirports()
	assertRoutesEquals(t, expectedRoutes)
	assertAirportsEquals(t, expectedAirports)
}

func TestLoadAirports(t *testing.T) {
	err := loadAirports([]byte(""))
	if err == nil {
		t.Error("No error while loading airports with invalid json content")
	}

	err = loadAirports([]byte("[]"))
	if err != nil {
		t.Log(err)
		t.Error("Error while loading airports with valid json content")
	}
	if len(airports) != 0 {
		t.Error("Airports array should be empty")
	}
}

func TestLoadPlanes(t *testing.T) {
	err := loadPlanes([]byte(""))
	if err == nil {
		t.Error("No error while loading airports with invalid json content")
	}

	err = loadPlanes([]byte("[]"))
	if err != nil {
		t.Log(err)
		t.Error("Error while loading planes with valid json content")
	}
	if len(airports) != 0 {
		t.Error("Planes array should be empty")
	}

	err = loadPlanes([]byte(`[
		{
			"model": "Airbus A380",
			"capacity": 853,
			"quantity": 100
		}
	]`))
	if err != nil {
		t.Log(err)
		t.Error("Error while loading planes with valid json content")
	}
	if len(planes) != 100 {
		t.Error("Planes array should have 100 elements")
	}
	if planes[0].Capacity != 853 {
		t.Error("Plane capacity should be 853")
	}
	if planes[0].Model != "Airbus A380" {
		t.Error("Plane name should be 'Airbus A380'")
	}
}
