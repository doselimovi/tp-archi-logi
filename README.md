# Projet d'Architectures Logicielles et Qualités

## Déploiement

Cloner le dépôt

```
git clone https://gitlab.isima.fr/doselimovi/tp-archi-logi.git
```

Lancer l'application avec Docker

```
docker build -t tp-archi-logi .
docker run -p 8080:8080 tp-archi-logi
```

## Utilisation

Une fois l'application lancée, elle écoute sur le port 8080. Ouvrir un navigateur et aller à l'adresse `localhost:8080`
