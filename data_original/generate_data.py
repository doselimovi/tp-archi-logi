import json

def generate_airports():
    f = open("data_original/airports.dat", "r")
    airports = []
    for line in f:
        line = line.split('\n')[0]
        data = line.split(',')
        try:
            airports.append({
                'id': int(data[0]),
                'name': data[1].replace('"', ''),
                'city': data[2].replace('"', ''),
                'country': data[3].replace('"', ''),
                'latitude': float(data[6]),
                'longitude': float(data[7])
            })
        except ValueError as err:
            print(err)
    f.close()

    f = open("data/airports.json", "w")
    f.write(json.dumps(airports, sort_keys=True, indent=4, separators=(',', ': ')))

def generate_routes():
    f = open("data_original/routes.dat", "r")
    routes = []
    i = 1
    for line in f:
        line = line.split('\n')[0]
        data = line.split(',')
        try:
            routes.append({
                'id': i,
                'source': int(data[3]),
                'dest':int(data[5])
            })
            i += 1
        except ValueError as err:
            print(err)
    f.close()

    f = open("data/routes.json", "w")
    f.write(json.dumps(routes, sort_keys=True, indent=4, separators=(',', ': ')))


def main():
    generate_routes()

if __name__ == "__main__":
    main()
